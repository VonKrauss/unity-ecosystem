﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
	public LayerMask layerMask;
	public Camera cam;

  void Start()
  {
  	cam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
  }

	// Update is called once per frame
	void Update()
	{
		if (Input.GetMouseButtonDown(0)) {

			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask)) {
				hit.transform.gameObject.SendMessage("OnClick");
			}
		}
	}
}
