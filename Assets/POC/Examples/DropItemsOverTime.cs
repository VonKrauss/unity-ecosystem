﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItemsOverTime : MonoBehaviour
{
	public float minInterval = 3;
	public float maxInterval = 10;
	public int maxNumItems = 3;
	public float throwForceMagnitude = 1;
	public GameObject item;

  void Start()
  {
  	StartCoroutine(NextIntervalCoroutine());
  }

  IEnumerator NextIntervalCoroutine () {
  	yield return new WaitForSeconds(Random.Range(minInterval, maxInterval));
  	Interval();
  }

  void Interval () {
  	// Spawn item in the world
  	SpawnItem();
  	// Start the timer for the next spawn
  	StartCoroutine(NextIntervalCoroutine());
  }

  public void SpawnItem () {
  	int numItems = Random.Range(1, maxNumItems);
  	Debug.Log("Spawning " + numItems + " item/s");

  	for (int i=0; i<numItems; i++) {
	  	var obj = Object.Instantiate(item, transform.position, Random.rotation);
	  	// Add random force to new object to toss it around
	  	Vector3 throwForce = new Vector3(Random.Range(-1f, 1f), Random.Range(3f,5f), Random.Range(-1f,1f)) * throwForceMagnitude;
	  	obj.GetComponent<Rigidbody>().AddForce(throwForce, ForceMode.Impulse);
  	}
  }
}
