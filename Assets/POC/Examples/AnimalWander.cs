﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AnimalBehaviour))]
[RequireComponent(typeof(NavMeshAgent))]
public class AnimalWander : MonoBehaviour
{
	public TerrainData td;

	public float wanderMinRadius = 2;
	public float wanderMaxRadius = 5;
	public float uneasiness = 1;

	NavMeshAgent agent;

	float t = 0;
	float nextWander = 1.5f;

	public AnimalBehaviour animal;


	void Start()
	{
		agent = GetComponent<NavMeshAgent>();
		animal = GetComponent<AnimalBehaviour>();
	}

	void Update()
	{
		if (t > nextWander && animal.idle) {
			Wander();
			nextWander += Random.Range(1.5f, 3f)/uneasiness;
		}

		t += Time.deltaTime;
	}

	void Wander () {
		// Pick a random point in space around the transform
		int tries = 0;
		while(tries <= 10) {
			int randomSignX = Random.Range(0f, 1f) > .5 ? 1 : -1;
			int randomSignY = Random.Range(0f, 1f) > .5 ? 1 : -1;
			float randomX = Random.Range(wanderMinRadius, wanderMaxRadius) * randomSignX;
			float randomZ = Random.Range(wanderMinRadius, wanderMaxRadius) * randomSignY;

			// Debug.Log("Random X: " + randomX + ", Random Z: " + randomZ);

			// Check if it's above water
			float newY = td.GetHeight((int)transform.position.x + (int)randomX, (int)transform.position.z + (int)randomZ);
			// Debug.Log("New Y: " + newY);
			if (newY > 1.14) {
				// SetDestination
				agent.SetDestination(transform.position + new Vector3(randomX, newY, randomZ));
				// animal.destinationGizmo.position = agent.destination;
				break;
			}
			tries ++;
		}
	}
}
