using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AnimalBehaviour))]
public class AnimalReproduction : MonoBehaviour
{
    [SerializeField]
    private bool gizmosON;

    public AnimalBehaviour animalBehaviour;
    public Animator animator;
    private NavMeshAgent agent;
    public GameObject animalContainer;

    public float fertility;
    public float sight;
    public float danceTime;

    public int babyAmount;
    public int ticksToAddAge;
    public int ticksToAddFertility;
    public int fertilityAmountToReproduce;
    public Color hornyColor;
    public Color normalColor;

    [HideInInspector]
    public string myTag;

    public bool firstGen = true;
    private bool babyBoomStabilized;
    private bool midPosDefined;

    private Vector3 midPos;
    public enum Mode { Chill, ReadyToReproduce, RagDoll }
    public Mode mode;

    private void Awake()
    {
        animalContainer = GameObject.FindGameObjectWithTag("AnimalContainer");
        animalBehaviour = GetComponent<AnimalBehaviour>();
        agent = GetComponent<NavMeshAgent>();
        myTag = gameObject.tag;
    }
    void Start()
    {
        if (firstGen)
            mode = Mode.Chill;
        else
            mode = Mode.RagDoll;

        fertility = 0;
        this.tag = myTag;
    }

    void Update()
    {
        if (!animalBehaviour.isChild && fertility < fertilityAmountToReproduce + 10)
            fertility += Time.deltaTime;

        if (fertility >= fertilityAmountToReproduce && animalBehaviour.age > animalBehaviour.adultAge)
            mode = Mode.ReadyToReproduce;

        if (mode == Mode.RagDoll && animalBehaviour.age >= (animalBehaviour.adultAge / 2))
            mode = Mode.Chill;

        switch (mode)
        {
            case Mode.RagDoll:
                GetComponent<AnimalWander>().enabled = false;
                agent.enabled = false;
                break;
            case Mode.Chill:
                animalBehaviour.idle = true;
                agent.enabled = true;
                GetComponent<AnimalWander>().enabled = true;
                if (gameObject.GetComponent<Rigidbody>() != null)
                    Destroy(gameObject.GetComponent<Rigidbody>());
                break;
            case Mode.ReadyToReproduce:
                Collider[] colliders = Physics.OverlapSphere(transform.position, sight);
                foreach (var item in colliders)
                {
                    if (item != GetComponent<Collider>() && item.CompareTag(myTag) && item.GetComponent<AnimalReproduction>().mode == Mode.ReadyToReproduce)
                    {
                        if (!midPosDefined)
                        {
                            midPosDefined = true;
                            midPos = Vector3.Lerp(transform.position, item.transform.position, 0.5f);
                        }

                        int randomInt = Random.Range(0, 1000);
                        if (randomInt <= 500)
                        {
                            GetComponent<AnimalWander>().enabled = false;
                            agent.SetDestination(midPos);

                            if (Vector3.Distance(transform.position, item.transform.position) <= 2f)
                            {
                                agent.speed = 0;
                                ReproductionDebuff();
                                item.GetComponent<AnimalReproduction>().ReproductionDebuff();
                                StartCoroutine(SpawnBaby());
                            }
                        }
                        else
                        {
                            item.GetComponent<AnimalWander>().enabled = false;
                            item.GetComponent<NavMeshAgent>().SetDestination(midPos);

                            if (Vector3.Distance(transform.position, item.transform.position) <= 2f)
                            {
                                item.GetComponent<AnimalReproduction>().agent.speed = 0;
                                ReproductionDebuff();
                                item.GetComponent<AnimalReproduction>().ReproductionDebuff();
                                item.GetComponent<AnimalReproduction>().StartCoroutine(SpawnBaby());
                            }
                        }
                    }
                    else
                        GetComponent<AnimalWander>().enabled = true;

                }
                break;
        }

        if (GetComponent<Rigidbody>() != null && babyBoomStabilized)
        {
            Invoke(nameof(BabyStabilize), 1f);
            if (GetComponent<Rigidbody>().velocity.magnitude < 0.1f)
                BabyStabilize();
        }

        if (fertility > fertilityAmountToReproduce)
        {
            for (int i = 0; i < transform.GetChild(0).childCount; i++)
            {
                if (transform.GetChild(0).GetChild(i).GetComponent<MeshRenderer>().material != null)
                    transform.GetChild(0).GetChild(i).GetComponent<MeshRenderer>().material.SetColor("_Color", hornyColor);
            }
        }
        else
        {
            for (int i = 0; i < transform.GetChild(0).childCount; i++)
            {
                if (transform.GetChild(0).GetChild(i).GetComponent<MeshRenderer>().material != null)
                    transform.GetChild(0).GetChild(i).GetComponent<MeshRenderer>().material.SetColor("_Color", normalColor);
            }
        }
    }

    public void ReproductionDebuff()
    {
        mode = Mode.Chill;
        fertility -= 20;
        animalBehaviour.idle = false;
        midPosDefined = false;
    }

    IEnumerator SpawnBaby()
    {
        animator.SetTrigger("danceMoment");
        agent.enabled = false;
        yield return new WaitForSeconds(danceTime);

        int randomAmountOfChilds = Random.Range(babyAmount - 2, babyAmount + 2);

        for (int i = 0; i < randomAmountOfChilds; i++)
        {
            GameObject myBaby = Instantiate(gameObject, transform.position, Quaternion.identity);
            myBaby.transform.parent = animalContainer.transform;
            var babyAnimalBehaviour = myBaby.GetComponent<AnimalBehaviour>();
            babyAnimalBehaviour.age = 0;
            babyAnimalBehaviour.Init();
            
            var babyAnimalReproduction = myBaby.GetComponent<AnimalReproduction>();
            // babyAnimalReproduction.StartCoroutine(BabyBoom());
            babyAnimalReproduction.firstGen = false;
        }

    }
    IEnumerator BabyBoom()
    {
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        Debug.Log("Rigidbody: " + rb);
        if (rb == null)
        {
            rb = gameObject.AddComponent<Rigidbody>();
        }
        rb.mass = 0.1f;
        rb.AddForce(new Vector3(Random.Range(-1, 1), 1, Random.Range(-1, 1)) * 10f, ForceMode.Impulse);
        yield return new WaitForSeconds(0.5f);
        babyBoomStabilized = true;
    }

    void BabyStabilize()
    {
        mode = Mode.Chill;
        Destroy(gameObject.GetComponent<Rigidbody>());
        babyBoomStabilized = false;
        transform.rotation = Quaternion.Euler(Vector3.zero);
    }

    private void OnDrawGizmos()
    {
        if (gizmosON)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(transform.position, sight);
        }
    }
}
