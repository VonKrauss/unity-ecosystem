﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
		public float movementSpeed = 10;

		public Transform camBoom;

		public float minZoom = 4f;
		public float maxZoom = 0.08f;
		public float zoomSpeed = -0.2f;

		// Start is called before the first frame update
		void Start()
		{
				
		}

		// Update is called once per frame
		void Update()
		{
				// Forwards/Backwards
    		float verticalMovement = (Input.GetAxis("Vertical")) * movementSpeed * Time.deltaTime;
        if (verticalMovement != 0) {
					camBoom.position += verticalMovement * transform.forward;
        }
				// Right/Left
				float horizontalMovement = (Input.GetAxis("Horizontal")) * movementSpeed * Time.deltaTime;
				if (horizontalMovement != 0) {
					camBoom.position += horizontalMovement * transform.right;
				}

				float zoomIncrement = Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
				if (zoomIncrement != 0) {
					camBoom.localScale += new Vector3(zoomIncrement, zoomIncrement, zoomIncrement);
					if (camBoom.localScale.x < maxZoom)
						camBoom.localScale = new Vector3(maxZoom,maxZoom,maxZoom);
					if (camBoom.localScale.x > minZoom)
						camBoom.localScale = new Vector3(minZoom,minZoom,minZoom);
				}
		}
}
