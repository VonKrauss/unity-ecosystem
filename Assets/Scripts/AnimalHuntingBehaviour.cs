using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AnimalBehaviour))]
public class AnimalHuntingBehaviour : MonoBehaviour
{
    public float huntRange;
    public float visualRange;
    public string preyTag;
    public LayerMask animalLayer;
    public bool onlyHuntYounglings = true;

    AnimalBehaviour animalBehaviour;
    public GameObject prey;

    // Start is called before the first frame update
    void Start()
    {
        animalBehaviour = GetComponent<AnimalBehaviour>();
    }

    // Update is called once per frame
    void Update()
    {
        Collider[] animalColliders = Physics.OverlapSphere(transform.position, visualRange, animalLayer);
        foreach (var animalCollider in animalColliders)
        {
            if (animalCollider.tag == preyTag && (!onlyHuntYounglings || (onlyHuntYounglings && GetComponent<AnimalBehaviour>().isChild)))
            {
                prey = animalCollider.gameObject;
            }
        }

        if (prey != null) {
            float distanceFromPrey = Vector3.Distance(transform.position, prey.transform.position);
            if (distanceFromPrey <= visualRange)
            {
                animalBehaviour.idle = false;
                animalBehaviour.agent.SetDestination(prey.transform.position);

                if (distanceFromPrey <= huntRange)
                {
                    prey.SendMessage(nameof(AnimalBehaviour.Die));
                    prey = null;
                }
            }
        } else  {
            animalBehaviour.idle = true;
        }
    }
}