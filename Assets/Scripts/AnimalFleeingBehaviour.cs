using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AnimalBehaviour))]
public class AnimalFleeingBehaviour : MonoBehaviour
{
    public float distanciaPeligro;
    public float incrementoVelocidad;
    public string depredadorTag;
    public LayerMask animalLayer;
    
    AnimalBehaviour animalBehaviour;
    public GameObject depredador;

    // Start is called before the first frame update
    void Start()
    {
        animalBehaviour = GetComponent<AnimalBehaviour>();
    }

    // Update is called once per frame
    void Update()
    {
        Collider[] animalColliders = Physics.OverlapSphere(transform.position, distanciaPeligro, animalLayer);
        foreach (var animalCollider in animalColliders)
        {
            if (animalCollider.tag == depredadorTag) depredador = animalCollider.gameObject;
        }

        if (depredador != null && Vector3.Distance(transform.position, depredador.transform.position) <= distanciaPeligro )
        {
            animalBehaviour.idle = false;
            animalBehaviour.agent.speed = animalBehaviour.baseSpeed + incrementoVelocidad;
			Vector3 movementDirection = transform.position - depredador.transform.position;
            animalBehaviour.agent.ResetPath();
            animalBehaviour.agent.SetDestination(transform.position + movementDirection.normalized);
            // animalBehaviour.destinationGizmo.position = animalBehaviour.agent.destination;
        }
        else
        {
            animalBehaviour.agent.speed = animalBehaviour.baseSpeed;
            animalBehaviour.idle = true;
        }
    }
}