﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericPopulator : MonoBehaviour
{
		public int amount = 10;

		public GameObject prefab;
		// public Terrain terrain;

		private TerrainData td;

		void Start()
		{
			td = FindObjectOfType<Terrain>().terrainData;
			PlaceTrees();   
		}

		void PlaceTrees() {
				for (int i=0; i<amount; i ++) {
					float _x = Random.Range(0,td.size.x);
					float _z = Random.Range(0,td.size.z);
					float _y = td.GetHeight((int)_x,(int)_z);
					// Debug.Log("Y: "+_y);
					if (_y > 1.14) {
						Vector3 pos = new Vector3(_x, _y, _z);
						GameObject tree = Instantiate(prefab, pos, Quaternion.identity);
						tree.transform.parent = transform;
						tree.transform.Rotate(new Vector3(0,Random.Range(0,360),0));
					}
				}
		}

		// Update is called once per frame
		void Update()
		{

		}
}
