﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantPopulator : MonoBehaviour
{
		public int plantsAmount = 80;

		public GameObject plantPrefab;
		public Terrain terrain;

		private TerrainData td;

		void Start()
		{
			td = terrain.terrainData;
			PlaceTrees();         
		}

		void PlaceTrees() {
				for (int i=0; i<plantsAmount; i ++) {
					PlacePlant();
				}
		}

		// Update is called once per frame
		void Update()
		{
			// Spawn a plant if one has been consumed
			if (GameObject.FindGameObjectsWithTag("Food").Length < plantsAmount)
				PlacePlant();
		}

		void PlacePlant() {
					float _x = Random.Range(0,td.size.x);
					float _z = Random.Range(0,td.size.z);
					float _y = td.GetHeight((int)_x,(int)_z);
					if (_y > -2) {
						Vector3 pos = new Vector3(_x, _y, _z);
						GameObject tree = Instantiate(plantPrefab, pos, Quaternion.identity);
						tree.transform.parent = transform;
					}
		}
}
