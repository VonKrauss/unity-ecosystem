using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalBehaviour : MonoBehaviour
{
    public float age = 0;
    public float adultAge = 30;
    public float babySpeed;
    public float adultSpeed;
    public float baseSpeed = 3;
    public bool idle = true;
	public NavMeshAgent agent;
    public bool isChild = false;

    public Transform destinationGizmo;
    
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        Init();
    }

    public void Init () {
        if (age == 0) {
            transform.localScale *= .5f;
            isChild = true;
        }
        baseSpeed = age == 0 ? babySpeed : adultSpeed;
        agent.speed = baseSpeed;
        // StartCoroutine(nameof(Grow));
    }

    void Update()
    {
        age += Time.deltaTime;
        if (isChild && age >= adultAge) {
            // transform.localScale = new Vector3(1,1,1);
            isChild = false;
            baseSpeed = adultSpeed;
            agent.speed = baseSpeed;
        }
    }

    public void Die () {
        Destroy(gameObject);
    }

    public IEnumerator Grow () {
        float size = 1;

        while (size < 3) {
            size = size + 0.01f;
            transform.localScale = Vector3.one * size;
            yield return new WaitForSeconds(.1f);
        }
    }
}