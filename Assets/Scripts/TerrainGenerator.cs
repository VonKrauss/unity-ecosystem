﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class TerrainGenerator : MonoBehaviour
{
	private Terrain t;
	private TerrainData td;

	public int w = 60;
	public int h = 60;
	public int d = 10;

	public float scale = 1;

	public int seed = 0;

    public bool steppedTerrain = false;
	public int steps = 5;

    public bool keepUpdated = false;

		void Start() {
			t = GetComponent<Terrain>();
			td = t.terrainData;
		}

    // Start is called before the first frame update
    void Update()
    {
        if (keepUpdated)
			td = GenerateTerrain(t.terrainData);
    }

    TerrainData GenerateTerrain(TerrainData td) {
    	td.heightmapResolution = w + 1;
    	td.size = new Vector3(w,d,h);
    	td.SetHeights(0, 0, GenerateHeights());
    	return td;
    }

    float[,] GenerateHeights() {
    	float[,] heights = new float[w,h];
    	for (int x = 0; x < w; x ++) {
	    	for (int y = 0; y < w; y ++) {
	    		heights[x,y] = CalculateHeight(x, y);
    		}
    	}
    	return heights;
    }

    float CalculateHeight(int x, int y) {
    	// float xCoord = (float)x/w * scale;
    	// float yCoord = (float)y/h * scale;

    	float xCoord = (float)x/w * scale;
    	float yCoord = (float)y/h * scale;

        float noise;
        if (steppedTerrain) {
            noise = Mathf.Floor(Mathf.PerlinNoise(xCoord + seed, yCoord + seed) * steps)/steps;
        } else {
            noise = Mathf.PerlinNoise(xCoord + seed, yCoord + seed);
        }
    	return noise;
    }
}
