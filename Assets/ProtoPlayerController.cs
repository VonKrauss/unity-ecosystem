﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtoPlayerController : MonoBehaviour
{
	public Camera cam;

	private string state = "idle";
	private Vector3 targetPos = new Vector3(0,0,0); 
	// Start is called before the first frame update
	public float distance = 50f;
	private LayerMask collisionLayer;
	private Vector3 mousePos = Vector3.zero;

	void Start()	
	{
		collisionLayer = LayerMask.GetMask("Ground");
	}

	// Update is called once per frame
	void Update()
	{
		switch (state) {
			case "idle":
			UpdateIdle();
			break;

			case "moving":
			UpdateMoving();
			break;
		}

		if (Input.GetKey(KeyCode.E)) {
			DrawRay();
		}
	}

	void DrawRay() {
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity)) {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.green);
		} else {
			Debug.DrawLine(transform.position, transform.TransformDirection(Vector3.forward) * 100, Color.red);
		}
	}

	//replace Update method in your class with this one
	void FixedUpdate ()
	{
		 //if mouse button (left hand side) pressed instantiate a raycast
		//create a ray cast and set it to the mouses cursor position in game
		Ray ray = cam.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, distance, collisionLayer)) 
		{
			mousePos = hit.point;
			//draw invisible ray cast/vector
			//log hit area to the console
			// Debug.Log(hit.point.x)
			if(Input.GetMouseButtonDown(0))
			{
				 targetPos = new Vector3(Mathf.Floor(hit.point.x) + .5f, hit.point.y, Mathf.Floor(hit.point.z) + .5f);
				 state = "moving";
			}
		}
		transform.LookAt(mousePos, Vector3.up);
	}

	void UpdateIdle () {
		{
			if (Input.GetKey("up")) {
				MoveTo(new Vector3(-1, 0, 0));
			}
			if (Input.GetKey("down")) {
				MoveTo(new Vector3(1, 0, 0));
			}
			if (Input.GetKey("left")) {
				MoveTo(new Vector3(0, 0, -1));
			}
			if (Input.GetKey("right")) {
				MoveTo(new Vector3(0, 0, 1));
			}
		}
	}

	void UpdateMoving () {
		transform.position = Vector3.Lerp(transform.position, targetPos, .3f);
		if (Vector3.Distance(transform.position, targetPos) < .01) {
			transform.position = targetPos;
			state = "idle";
		}
	}

	void MoveTo(Vector3 pos)
	{
		targetPos = transform.position + pos;
		state = "moving";
	}
}
