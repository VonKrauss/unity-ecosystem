using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockBehaviour : MonoBehaviour
{
    void Start () {
        StartCoroutine(Die());
    }

    void OnCollisionEnter (Collision collision) {
        // Debug.Log("Collision with " + collision.gameObject.name);

        Vector3 otherPosition = collision.collider.transform.position;
        Vector3 relativeVelocity = collision.relativeVelocity;
        Vector3 contact = collision.GetContact(0).point;
    }

    public IEnumerator Die () {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
}