using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowRock : MonoBehaviour
{
    // Si presionamos la tecla "lanzar piedra",
    // una piedra sale disparada desde la posición del jugador
    // con una (dirección y momento) X y un ángulo vertical Y

    public GameObject rock;
    public float speed;
    public float verticalMomentum;
    Camera cam; 

    void Start () {
        cam = Camera.main;
    }

    void Update () {
        if (Input.GetMouseButtonDown(1)) {
        	Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        	RaycastHit hit;
        	if (Physics.Raycast(ray, out hit)) {
                Vector3 direction = (hit.point - transform.position).normalized;
                Vector3 velocity = direction * speed + (Vector3.up * verticalMomentum);

                var newRock = Instantiate(rock, transform.position + Vector3.up * 0.5f, Random.rotation);
                newRock.GetComponent<Rigidbody>().AddForce(velocity, ForceMode.Impulse);
        	}            
        }
    }
}
